//Projet Agile box
#include <ESP8266WiFi.h>
#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>
#include <Adafruit_NeoPixel.h>
#include <Timemark.h>
#ifndef PSTR
#define PSTR // Make Arduino Due happy
#endif

#define PIN D8
#define BUZ D6
#define LIGHT A0


Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(32, 8, PIN,  NEO_MATRIX_TOP + NEO_MATRIX_LEFT + NEO_MATRIX_ROWS + NEO_MATRIX_ZIGZAG, NEO_GRB + NEO_KHZ800);

uint16_t colorsLogo[] = {
  matrix.Color(255, 0, 0), matrix.Color(0, 255, 0), matrix.Color(255, 210, 0), matrix.Color(0, 0, 255), matrix.Color(255, 0, 255), matrix.Color(0, 255, 255), matrix.Color(255, 255, 255), matrix.Color(91, 68, 43), matrix.Color(0, 0, 0)
};

static unsigned char play[] = {0x00, 0x00, 0x10, 0x18, 0x1c, 0x18, 0x10, 0x00};
static unsigned char rond[] = {0x00, 0x7e, 0xff, 0xff, 0xff, 0xff, 0xff, 0x7e};
static unsigned char kk[] = {0x04, 0x00, 0x18, 0x3c, 0x3c, 0x7e, 0x7e, 0x00};
static unsigned char twitter[] = {0x04, 0x8f, 0x6e, 0x7e, 0x7e, 0x3c, 0x38, 0x30};
static unsigned char sun[] = {0x24, 0x00, 0xbd, 0x7e, 0x7e, 0x7e, 0x00, 0x00};
static unsigned char mask[] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
static unsigned char cloud[] = {0x00, 0x00, 0x00, 0x06, 0x6f, 0xef, 0xff, 0x7e};



int lux = 0;
Timemark pauseBuz(500);
int BUZState = LOW;
void setup() {
  //pas besoin du wifi désactivation du soft
  WiFi.mode( WIFI_OFF );
  WiFi.forceSleepBegin();
  //ESP.deepSleep(1000, WAKE_RF_DISABLED);
  //ESP.wdtDisable();
  // initialize serial:
  Serial.begin(9600);
  matrix.begin();
  matrix.setTextWrap(false);
  matrix.setBrightness(40);
  matrix.setTextColor(matrix.Color(255, 0, 0));


  pinMode(BUZ, OUTPUT);

}

int x    = matrix.width();
int pass = 0;
String logo = "";
String texte = ":)";
String r = "255";
String g = "";
String b = "";
int bip = 0;

void loop() {

  //permet de relacher le thread principal pour traiter le hard
  delay(1);
  yield();
  // if there's any serial available, read it:
  /* while (Serial.available() > 0) {

     logo = Serial.readStringUntil(';');
     texte = Serial.readStringUntil(';');
     r = Serial.readStringUntil(';');
     g = Serial.readStringUntil(';');
     b = Serial.readStringUntil(';');
     //on double le nombre de bip car 1 bip = 1 temps allumé + 1 temps éteind
     bip = atoi(Serial.readStringUntil('\0').c_str()) * 2;

    }*/

  //Serial.println(analogRead(LIGHT));

  if (analogRead(LIGHT) > 500)
  {


    matrix.fillScreen(0);

    //en pixels 1 char  = 6pix
    int tailleChaine = texte.length() * 6;

    //Gestion de la position du curseur
    //en fonction du logo de la taille du texte on déduit s'ily a besoin d'un défilement
    if (tailleChaine < matrix.width() && logo == "")
    {
      x = 0;
    } else if (tailleChaine + 7 < matrix.width() && logo != "") //7 car avec 8 on perd souvent un caractère pour 1 pixel près
    {
      x = 8;
    } else {
      if (--x < -tailleChaine) {
        x = matrix.width();

      }
      //pour l'animation fluide du texte défilant
      delay(30);
    }
    matrix.setCursor(x, 0);
    matrix.setTextColor(matrix.Color(atoi(r.c_str ()), atoi(g.c_str ()), atoi(b.c_str ())));
    matrix.print(texte);



    //affichage du logo par dessus le texte
    if (logo != "")
    {

      if (logo == "youtube")
      {
        matrix.drawBitmap(0, 0,  mask, 8, 8, colorsLogo[8]);
        matrix.drawBitmap(0, 0,  rond, 8, 8, colorsLogo[0]);
        matrix.drawBitmap(0, 0,  play, 8, 8, colorsLogo[6]);
      }
      else if (logo == "couvert")
      {
        matrix.drawBitmap(0, 0,  mask, 8, 8, colorsLogo[8]);
        matrix.drawBitmap(0, 0,  sun, 8, 8, colorsLogo[2]);
        matrix.drawBitmap(0, 0,  cloud, 8, 8, colorsLogo[6]);
      }
      else if (logo == "soleil")
      {
        matrix.drawBitmap(0, 0,  mask, 8, 8, colorsLogo[8]);
        matrix.drawBitmap(0, 0,  sun, 8, 8, colorsLogo[2]);
      }
      else if (logo == "nuage")
      {
        matrix.drawBitmap(0, 0,  mask, 8, 8, colorsLogo[8]);
        matrix.drawBitmap(0, 0,  cloud, 8, 8, colorsLogo[6]);
      }
      else if (logo == "twitter")
      {
        matrix.drawBitmap(0, 0,  mask, 8, 8, colorsLogo[8]);
        matrix.drawBitmap(0, 0,  twitter, 8, 8, colorsLogo[3]);
      }

    }


    matrix.show();

    //le temp du buzzer est réalisé sans blocage de l'IC
    if (pauseBuz.expired()) {
      bip--;
      pauseBuz.stop();
    }
    if (bip > 0 && !pauseBuz.running()  )
    {
      digitalWrite(BUZ, BUZState = BUZState ? LOW : HIGH);
      pauseBuz.start();
    }

  }
  else //c'est la pénombre on éteind la box, tout le monde au lit!
  {
    matrix.clear();
    matrix.show();
    digitalWrite(BUZ, LOW);

  }

}
