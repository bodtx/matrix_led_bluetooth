

/* Create a WiFi access point and provide a web server on it. */

#include <ESP8266WiFi.h>
#include <EEPROM.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>
#include <Adafruit_NeoPixel.h>


#define PIN D8
#define BUZ D6
#define LIGHT A0


Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(32, 8, PIN,  NEO_MATRIX_TOP + NEO_MATRIX_LEFT + NEO_MATRIX_ROWS + NEO_MATRIX_ZIGZAG, NEO_GRB + NEO_KHZ800);

uint16_t colorsLogo[] = {
  matrix.Color(255, 0, 0), matrix.Color(0, 255, 0), matrix.Color(255, 210, 0), matrix.Color(0, 0, 255), matrix.Color(255, 0, 255), matrix.Color(0, 255, 255), matrix.Color(255, 255, 255), matrix.Color(91, 68, 43), matrix.Color(0, 0, 0)
};

static unsigned char play[] = {0x00, 0x00, 0x10, 0x18, 0x1c, 0x18, 0x10, 0x00};
static unsigned char rond[] = {0x00, 0x7e, 0xff, 0xff, 0xff, 0xff, 0xff, 0x7e};
static unsigned char kk[] = {0x04, 0x00, 0x18, 0x3c, 0x3c, 0x7e, 0x7e, 0x00};
static unsigned char twitter[] = {0x04, 0x8f, 0x6e, 0x7e, 0x7e, 0x3c, 0x38, 0x30};
static unsigned char sun[] = {0x24, 0x00, 0xbd, 0x7e, 0x7e, 0x7e, 0x00, 0x00};
static unsigned char mask[] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
static unsigned char cloud[] = {0x00, 0x00, 0x00, 0x06, 0x6f, 0xef, 0xff, 0x7e};



int lux = 0;
//Timemark pauseBuz(500);
int BUZState = LOW;

#ifndef APSSID
#define APSSID "AE_Box"
#define APPSK  "AE4EVERACOSS"
#endif

/* Set these to your desired credentials. */
const char *ssid = APSSID;
const char *password = APPSK;

ESP8266WebServer server(80);

int x    = matrix.width();
int pass = 0;
String logo = "";
String texte = "";
String r = "255";
String g = "255";
String b = "255";
String couleur = "" ;
int bip = 0;
boolean lumiere=false;





/* Just a little test message.  Go to http://192.168.4.1 in a web browser
   connected to this access point to see it.
*/
void handleRoot() {
  server.send(200, "text/html", "<!DOCTYPE html><html><head><meta charset='UTF-8'><style>body {  font: normal 18px/1.5 'Fira Sans', 'Helvetica Neue', sans-serif;  background: #3AAFAB;  color: #fff;  padding: 50px 0;  }input, label{font-family: sans-serif;font-size: 5em;line-height: 1.15;margin: 0;}      .box {        display: flex;   flex-direction: column;   justify-content: space-around;    height: 100vh;    align-items: center;      }   .champs{    width: 90%;   height: 100px;    }   .couleur{   height: 200px;    width: 200px;   }   .radio{   width: 100px;height: 100px;   }      .col {        flex: 1 0 auto;          }</style></head><body>            <form class='box' method='post' action='/maj'>      <div class='col'>       <input class='radio' type='radio' id='aucun' checked name='logo' value=''>        <label class='champs' for='aucun'>aucun</label><br>       <input class='radio' type='radio' id='twitter' name='logo' value='twitter'>       <label for='twitter'>twitter</label><br>        <input class='radio' type='radio' id='youtube' name='logo' value='youtube'>       <label for='youtube'>youtube</label><br>        <input class='radio' type='radio' id='nuage' name='logo' value='nuage'>       <label for='nuage'>nuage</label><br>        <input class='radio' type='radio' id='soleil' name='logo' value='soleil'>       <label for='soleil'>soleil</label> <br>       <input class='radio' type='radio' id='couvert' name='logo' value='couvert'>       <label for='couvert'>couvert</label>      </div>      <div class='col'><input class='champs' type='text' name='texte' placeholder='texte défilant'/></div>      <div class='col'><input class='couleur' type='color' name='couleur'/></div>     <div class='col'><input class='champs' type='number' name='bip'  value='0' min='0' max='10'/></div>     <div class='col'><input  type='submit' value='Go'/></div>     </form>     </body></html>");

}

void affecteCouleurRGB() {
  Serial.println("enter maj");
  char charbuf[8];
  couleur.toCharArray(charbuf, 8);
  long int rgb = strtol(charbuf, 0, 16); //=>rgb=0x001234FE;
  r = (String)(rgb >> 16);
  g = (String)(rgb >> 8);
  b = (String)(rgb);
}

void handleMaj() {

  Serial.println("enter maj");
  texte = server.arg("texte");
  logo = server.arg("logo");
  couleur = server.arg("couleur").substring(1, 7);
  bip = server.arg("bip").toInt() * 2; //on double le nombre de bip car 1 bip = 1 temps allumé + 1 temps éteind


  Serial.println(couleur);
  affecteCouleurRGB();

  yield();

  Serial.println("handleRoot");
  handleRoot();
  yield();
  //Sauvegarde EEPROM
  EEPROM.begin(512);
  int addrEEprom = 0;
  for (int i = 0; i < couleur.length(); i++)
  {
    EEPROM.write(addrEEprom++, couleur[i]);
  }
  yield();
  for (int j = 0; j < texte.length(); j++)
  {
    EEPROM.write(addrEEprom++, texte[j]);

  }
  EEPROM.write(addrEEprom, ';');
  EEPROM.end();
  

  Serial.println("commit eeprom");
  //delay(100);
  yield();
}

void setup() {
  Serial.begin(115200);
  pinMode(BUZ, OUTPUT);
  pinMode(A0, INPUT);




  //Serial.println();
  //Serial.print("Configuring access point...");
  /* You can remove the password parameter if you want the AP to be open. */
  WiFi.softAP(ssid, password);

  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);
  server.on("/", HTTP_GET, handleRoot);
  server.on("/maj", HTTP_POST, handleMaj);
  server.begin();
  //Serial.println("HTTP server started");
  yield();
  matrix.begin();
  matrix.setTextWrap(false);
  matrix.setBrightness(40);
  matrix.setTextColor(matrix.Color(255, 0, 0));

  EEPROM.begin(512);  //512 octets, la loose mais on peut y écrire 1 millions de fois.
  int addrEEprom = 0;

  if (EEPROM.read(0) != '0') {


    for (int i = 0; i < 6; i++)
    {
      couleur = couleur + char(EEPROM.read(i));
      addrEEprom++;
    }
    affecteCouleurRGB();
    
    while (char(EEPROM.read(addrEEprom)) != ';')
    {
      texte = texte + char(EEPROM.read(0 + addrEEprom));
      addrEEprom++;
      if (addrEEprom > 512) {
        break;
      }
    }
  }

  EEPROM.end();



  




}

void loop() {

 yield();
 delay(30);
  server.handleClient();

  if (analogRead(LIGHT) > 500)
  {
    lumiere=true;
    matrix.fillScreen(0);

    //en pixels 1 char  = 6pix
    int tailleChaine = texte.length() * 6;

    //Gestion de la position du curseur
    //en fonction du logo de la taille du texte on déduit s'ily a besoin d'un défilement
    if (tailleChaine < matrix.width() && logo == "")
    {
      x = 0;
    } else if (tailleChaine + 7 < matrix.width() && logo != "") //7 car avec 8 on perd souvent un caractère pour 1 pixel près
    {
      x = 8;
    } else {
      if (--x < -tailleChaine) {
        x = matrix.width();

      }
      //pour l'animation fluide du texte défilant


    }
    matrix.setCursor(x, 0);
    matrix.setTextColor(matrix.Color(atoi(r.c_str ()), atoi(g.c_str ()), atoi(b.c_str ())));
    matrix.print(texte);



    //affichage du logo par dessus le texte
    if (logo != "")
    {

      if (logo == "youtube")
      {
        matrix.drawBitmap(0, 0,  mask, 8, 8, colorsLogo[8]);
        matrix.drawBitmap(0, 0,  rond, 8, 8, colorsLogo[0]);
        matrix.drawBitmap(0, 0,  play, 8, 8, colorsLogo[6]);
      }
      else if (logo == "couvert")
      {
        matrix.drawBitmap(0, 0,  mask, 8, 8, colorsLogo[8]);
        matrix.drawBitmap(0, 0,  sun, 8, 8, colorsLogo[2]);
        matrix.drawBitmap(0, 0,  cloud, 8, 8, colorsLogo[6]);
      }
      else if (logo == "soleil")
      {
        matrix.drawBitmap(0, 0,  mask, 8, 8, colorsLogo[8]);
        matrix.drawBitmap(0, 0,  sun, 8, 8, colorsLogo[2]);
      }
      else if (logo == "nuage")
      {
        matrix.drawBitmap(0, 0,  mask, 8, 8, colorsLogo[8]);
        matrix.drawBitmap(0, 0,  cloud, 8, 8, colorsLogo[6]);
      }
      else if (logo == "twitter")
      {
        matrix.drawBitmap(0, 0,  mask, 8, 8, colorsLogo[8]);
        matrix.drawBitmap(0, 0,  twitter, 8, 8, colorsLogo[3]);
      }

    }

    //Serial.println("go");

    matrix.show();

    //Serial.println(bip);
    //le temp du buzzer est réalisé sans blocage de l'IC
   /* if (pauseBuz.expired()) {
      bip--;
      pauseBuz.stop();
    }*/
    while (bip > 0 /*&& !pauseBuz.running()*/  )
    {
      digitalWrite(BUZ, BUZState = BUZState ? LOW : HIGH);

      delay(300);
      bip--;
      //pauseBuz.start();
    }
  }
  //Ici on ne passe pas à chaque fois dedans, ça fait planter le wifi, une fois suffit
  else if(lumiere==true) //c'est la pénombre on éteind la box, tout le monde au lit!
  {
   
    matrix.clear();
    matrix.show();
   
    digitalWrite(BUZ, LOW);
    lumiere=false;

  }
}
