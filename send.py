#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Permet d\'envoyer du contenu à afficher à la box.
Author: Alexis Krier <alexis.krier@toujours.top>
$Id: send.py 424 2020-08-24 03:35:54Z albert $
"""

import sys
import argparse
import bluetooth


addr = "00:14:03:05:59:80"


parser = argparse.ArgumentParser(description='Permet d\'envoyer du contenu à afficher à la box')
parser.add_argument('--logo', help='logo à afficher dans le choix' , choices=['soleil', 'youtbe'])
parser.add_argument('--texte', help='texte à afficher, le défilement est automatique', required=True)
parser.add_argument('--couleur', help='couleur RGB à afficher ex 0 255 45', nargs=3, type=int, choices=range(0, 256))
parser.add_argument('--bip', help='indique si le bip doit être activé ainsi que le nombre de sonneries', type=int, choices=range(1, 10))

args = parser.parse_args()
couleurs="255;255;255"
if args.couleur:
    couleurs=str(args.couleur[0])+";"+str(args.couleur[1])+";"+str(args.couleur[2])
data = str(args.logo or '')+";"+args.texte+";"+couleurs+";"+str(args.bip or '')


print(bluetooth.lookup_name(addr, timeout=10));


# print("Connecting to \"{}\" on {}".format(name, host))

# Create the client socket

sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
sock.connect((addr, 1))
print("Connecté. ")
sock.send(data)
print ('data envoyé: %s'%data)

sock.close()
